# SMCovid19

Reports&Files Repository for SM_COVID19 (SM-COVID-19) app by SoftMining

On April 1st, **SM_COVID19** - developed by Softmining - has been published on Google Play Store (App changed name on April 12).

**There is no explicit evidence about which institution has officially provided the required authorizations (according to COVID-19 app guidelines).**

**The former Italian Ministry for Digital Innovation - Paola Pisano - never replied to the requests for checks and clarifications.**

**Private checks have anyway identified the Campania Region as the institutional sponsor behind the authorization for the publication of such app on the Google Play Store.**

It's a centralized solution. Every 60 seconds or 5 minutes (according to outdoors/indoors mode) it checks proximity/interactions with other peers (via Bluetooth/BLE) and retrieves the device location (Wi-Fi and GPS - if allowed by the user). Then it uploads such data to a Firebase instance. 

However, according to recent checks, since June 2020 the app started to collect also the rolling proximity identifiers of GAEN contact tracing apps & uploaded ALL the BLE interactions with other devices.

Beta releases (before the submission to Play Store) had critical issues about data validation = database tampering with fake user & location data. Until now, production releases haven't been enabled to check past interactions with those users, that have been officially confirmed as infected by Health Authorities.

There are still issues with User ID generation (simple concatenation of Android Device ID until June 2020 & poorly hashed later) and unneeded permissions.

More than 50k downloads from Google Play Store (more than 100k according to the publisher).

**Latest release update (5.1 - July 2020) has been reported to Google Play Developer Support because of malicious code detections.** 

Play Store | https://play.google.com/store/apps/details?id=it.softmining.projects.covid19.savelifestyle
-----------|-------------------------------------------------------------------------------------------
Sources | Unreleased
Website | https://www.smcovid19.org/

- APKLAB Analysis

Build | Link
------|-----
5.1  #35 | https://apklab.io/apk.html?hash=b5728080de8a6a1bdb8c3a2ff52ab88f81438415e0ea83b6c56c5b49bdec419e
5.0  #34 | https://apklab.io/apk.html?hash=ca044e06b41e883a9eeb8e311fcc6f28c68d35c477cbd97d335a35d20dd3b046
4.5  #33 | https://apklab.io/apk.html?hash=f1634d5ca7ac829b47d233c911afd52e024e7e95f8b33ffc3565adb3c92d8e78
4.4  #32 | https://apklab.io/apk.html?hash=5aa062cafe78ff07f5537475816b9e2d4d516bbf855d86d12c155eba027f6be3
4.3  #31 | https://apklab.io/apk.html?hash=9fc80059939cc7ea693ca4e395da55113013facc11f82e157f0ed95a2142b26a
4.2  #30 | https://apklab.io/apk.html?hash=1e52a2428e485fb0ff2f461bb053e5dbce1f526c64b70c5a5cbcbebeab88fd8a
4.1  #28 | https://apklab.io/apk.html?hash=d2b5546d27bdbbe044063997cfe3292fdafafea4a55eeeadd77e6c236b429873
4.0  #27 | https://apklab.io/apk.html?hash=5225d7722a401aa9d89da99b8cd342d087c6b4ab40a0d1b15fe5b74a84aecee1
3.9b #26 | https://apklab.io/apk.html?hash=6f46eef9c0c63436323aff9f031e5b462756d544192e44b0d734450e066f0b06
3.9  #25 | https://apklab.io/apk.html?hash=5bcbab60139be51484a46b888a51b19a9eaa278338b8adcaac512dfb804a3923
3.8  #24 | https://apklab.io/apk.html?hash=bf815c40391c3a615130a2579b86e22ee3f903dad371dd7cc36a3ba8d02575f4
3.6  #22 | https://apklab.io/apk.html?hash=5b012eb6e0cef92ffce352a84edbe4a9833d1dd6a507554efc2030bd34b7fc11
3.5  #21 | https://apklab.io/apk.html?hash=cc930ca16d75cb3b2a66fe7fc69a2fc66e05f37cae5ab39694096f0eb317131e
3.4  #20 | https://apklab.io/apk.html?hash=b060d73007a3b2969f9a6a848b1e4bc77a4d566663344dd594eaeb2f6142c4d6
3.3  #19 | https://apklab.io/apk.html?hash=74610ea4d89d1a01ccfcc64adb65245526c36fbbec5a5b569ece21cb230f3cf2
3.2  #18 | https://apklab.io/apk.html?hash=1db0fa65cba95b9e5570e8ff12bdd3db94f11271ca3293b4f166ac36921241c7
3.1  #16 | https://apklab.io/apk.html?hash=a82f5c9abea3214adc93ee811c43fd31355982fc824ffc4d813072ece5ab6250
2.6  #14 | https://apklab.io/apk.html?hash=98a4500cbe144b74640394005c3781c4633c1dbbd235e64b7363e7008a941ebf
2.5  #13 | https://apklab.io/apk.html?hash=e7d9d846e3579d467ece5c2dc50a6f54a19d223caf131a6ab4f2bd28a0dad63c
2.4  #12 | https://apklab.io/apk.html?hash=96f8f365f0e259d8ec3fb1c959359a85c5ecec576ef2fd1bf9e3813a2a59a4fd

- JoeSandbox Report

Build | Link
------|-----
5.1 #35 | https://www.joesandbox.com/analysis/432085/0/html
[...]   | 
3.9 #25 | https://www.joesandbox.com/analysis/236289/0/html
3.8 #24 | https://www.joesandbox.com/analysis/235357/0/html
3.6 #22 | https://www.joesandbox.com/analysis/229829/0/html
3.5 #21 | https://www.joesandbox.com/analysis/228751/0/html
3.4 #20 | https://www.joesandbox.com/analysis/227111/0/html
3.3 #19 | https://www.joesandbox.com/analysis/226714/0/html
3.2 #18 | https://www.joesandbox.com/analysis/226247/0/html
3.1 #16 | https://www.joesandbox.com/analysis/225349/0/html
2.6 #14 | https://www.joesandbox.com/analysis/223624/0/html
2.5 #13 | https://www.joesandbox.com/analysis/223468/0/html
2.4 #12 | https://www.joesandbox.com/analysis/223210/0/html

- Additional Malicious Code detections for release 5.1 #35

Website | Link
--------|-----
VirusTotal | https://www.virustotal.com/gui/file/b5728080de8a6a1bdb8c3a2ff52ab88f81438415e0ea83b6c56c5b49bdec419e/detection
Pithus | https://beta.pithus.org/report/b5728080de8a6a1bdb8c3a2ff52ab88f81438415e0ea83b6c56c5b49bdec419e
Triage | https://tria.ge/210609-5k9g3dm5j6